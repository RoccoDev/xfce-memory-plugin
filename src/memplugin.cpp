// Copyright (c) 2019 RoccoDev
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#include <iostream>
#include "memory/memory.h"

int main()
{
    readmem::MemoryData mem = readmem::read();
    int perc = mem.memoryFree * 100 / mem.memoryMax;
    std::cout << perc << '%';
    return 0;
}