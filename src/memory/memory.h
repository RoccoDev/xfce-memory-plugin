// Copyright (c) 2019 RoccoDev
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#pragma once

namespace readmem
{
struct MemoryData
{
    int memoryFree;
    int memoryMax;
};

MemoryData read();
} // namespace readmem