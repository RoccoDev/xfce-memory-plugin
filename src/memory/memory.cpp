// Copyright (c) 2019 RoccoDev
//
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

#include "memory.h"
#include <fstream>
#include <memory>
#include <regex>
#include <iostream>
#include <vector>

using namespace std;

void matchAndAssign(string const &in, regex const &regexIn, int *assign)
{
    string matches = regex_replace(in, regexIn, "");
    *assign = stoi(matches);
}

readmem::MemoryData readmem::read()
{
    auto filePtr = make_unique<ifstream>("/proc/meminfo");
    int free = -1;
    int max = -1;

    for (string line; getline(*filePtr, line);)
    {
        if (free == -1)
        {
            int pos = line.find("MemFree:");
            if (pos != -1)
            {
                matchAndAssign(line, regex(R"([^\d])"), &free);
            }
        }

        if (max == -1)
        {
            int pos = line.find("MemTotal:");
            if (pos != -1)
            {
                matchAndAssign(line, regex(R"([^\d])"), &max);
            }
        }

        if (free != -1 && max != -1)
        {
            break;
        }
    }

    return {free, max};
}